describe('Efiware.com', function() {
    it('Should have a title tag with Efiware included', function() {
        cy.visit('https://efiware.com/');

        cy.get('title').contains('Efiware');
    });

    it('Should navigate to contact page', function() {
        cy.visit('https://efiware.com/');

        cy.get('.field--type-link a[href="/kontakt-os"]').click({force: true});

        cy.url().should('include', 'kontakt-os');
    })
});
